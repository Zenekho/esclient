package com.cj3.elasticsclient.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cj3.elasticsclient.dao.MsgRepository;
import com.cj3.elasticsclient.model.Msg;

@RestController
@CrossOrigin(origins = { "*", "http://localhost:4200" })
public class MsgController {

	@Autowired
	private MsgRepository msgRepo;

	@GetMapping("/all")
	public List<Msg> getAllMessages() {
		List<Msg> target = new ArrayList<Msg>();
		msgRepo.findAll().forEach(target::add);
		return target;
	}

	@GetMapping("/searchByEmetteur/{emetteur}")
	public List<Msg> getMsgBySenderName(@PathVariable(name = "emetteur") String emetteur) {

		return msgRepo.findByEmetteurContainingIgnoreCase(emetteur);

	}

	@PostMapping("/new")
	public Msg addNewMessage(@RequestBody Msg message) {

		return msgRepo.save(message);

	}

}
