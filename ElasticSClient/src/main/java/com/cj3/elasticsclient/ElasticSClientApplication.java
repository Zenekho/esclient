package com.cj3.elasticsclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cj3.elasticsclient.dao.MsgRepository;

@SpringBootApplication
public class ElasticSClientApplication implements CommandLineRunner {

	@Autowired
	private MsgRepository msgRepo;

	public static void main(String[] args) {
		SpringApplication.run(ElasticSClientApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		/*
		 * // msg0 Msg msg = new Msg();
		 * msg.setContenu("la Reunion du lundi est décalé à 13h");
		 * msg.setEmetteur("Christian"); List<String> recepteurs = new
		 * ArrayList<String>(); recepteurs.add("Hamadi"); recepteurs.add("Christophe");
		 * recepteurs.add("Ezekiel");
		 * 
		 * msg.setRecepteurs(recepteurs); msgRepo.save(msg);
		 * 
		 * // msg1 Msg msg1 = new Msg(); msg1.setContenu("Bonne année 2022 !!!");
		 * msg1.setEmetteur("Christophe"); List<String> recepteurs1 = new
		 * ArrayList<String>(); recepteurs1.add("Christian"); recepteurs1.add("Hamadi");
		 * recepteurs1.add("Ezekiel"); recepteurs1.add("Thomas");
		 * 
		 * msg1.setRecepteurs(recepteurs1); msgRepo.save(msg1);
		 * 
		 * // msg2 Msg msg2 = new Msg();
		 * msg2.setContenu("Bien recu pour reunion et bonne année ");
		 * msg2.setEmetteur("Hamadi"); List<String> recepteurs2 = new
		 * ArrayList<String>(); recepteurs2.add("Christian");
		 * recepteurs2.add("Christophe");
		 * 
		 * msg2.setRecepteurs(recepteurs2); msgRepo.save(msg2);
		 */

		// msgRepo.deleteById("EohorH4BirNd08uu4OMf");
	}

}
