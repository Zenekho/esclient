package com.cj3.elasticsclient.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Document(indexName = "msgindex")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Msg {

	@Id
	private String id;
	private String contenu;
	private String emetteur;
	private List<String> recepteurs;

}
