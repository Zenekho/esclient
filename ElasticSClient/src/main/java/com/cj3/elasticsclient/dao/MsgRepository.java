package com.cj3.elasticsclient.dao;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.cj3.elasticsclient.model.Msg;

@Repository
public interface MsgRepository extends ElasticsearchRepository<Msg, String> {

	public List<Msg> findByEmetteurContainingIgnoreCase(String emetteur);

}
