import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MonService } from 'src/services/monservice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'esClientAngular';

  messages:any[];


  constructor(private monservice:MonService, private router:Router)
  {
   
  }
  ngOnInit(): void {

    this.monservice.searchAll().subscribe(data=>{  
     
      this.messages = data;
          
      }, err=>{
        
        
       console.log(err);
      
     
      
      });
    
  }

  searchSender(emetteur:string)
  {
    if(emetteur.toString().trim().length===0)
    this.monservice.searchAll().subscribe(data=>{  
     
      this.messages = data;
          
      }, err=>{
        
        
       console.log(err);
      
     
      
      });

    else 

    this.monservice.searchByEmetteur(emetteur.toString().trim()).subscribe(data=>{  
     
    this.messages = data;
        
    }, err=>{
      
      
     console.log(err);
    
   
    
    });

  }
}
