import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";



@Injectable()
export class MonService{

    private host:string="http://localhost:8080";

    constructor(private http:HttpClient){

    }

    searchByEmetteur(sender:string ):Observable<any>{

        return this.http.get<any>(this.host+"/searchByEmetteur/"+sender,{headers:new HttpHeaders({"responseType":"text"})});
    }

    searchAll():Observable<any>{

        return this.http.get<any>(this.host+"/all",{headers:new HttpHeaders({"responseType":"text"})});
    }
}